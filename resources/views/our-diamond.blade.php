@extends('layout')
@section('custom_style')

@endsection
@section('content')
<div>
  @include('partial.page-header', [
      'pageTitle' => "The page are not available for now",
      'pageDesc' => "",
  ])
</div>
<div class="row">
  <div class="col">
    @include('partial.banner', ['imageUrl' => "/img/banner-default.jpg"])
  </div>
</div>
@endsection