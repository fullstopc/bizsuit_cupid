@extends('layout')
@section('content')
<div>
    @include('partial.page-header', [
        'pageTitle' => "EDUCATION",
        'pageDesc' => "",
    ])
</div>
<div class="row">
  <div class="col">
    @include('partial.banner', ['imageUrl' => "/img/banner-default.jpg"])
  </div>
</div>
<div class='container pt-4'>
    <?php if (count($blogs) == 0) { ?>
    <div class="row">
        <div class="col">
            <div class="d-flex align-items-center justify-content-center p-4 border">
                <span>No Data</span>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <?php if (count($blogs) > 0) { ?>
        <div class="blog-list">
            @foreach($blogs as $blog)
                <div class="blog-item">
                    <div class="blog-image">
                        <img src="/storage/content/{{$blog['id']}}/{{$blog['image']}}" class="img-fluid" onerror="this.onerror=null;this.src='/img/image-not-found.png';" />    
                    </div>
                    <div class="blog-content">
                        <a class="blog-title" href='/education/{{$blog['url']}}'>{!!$blog["title"]!!}</a>
                        <div class="blog-desc">{!!$blog["desc"]!!}</div>
                    </div>
                </div>
            @endforeach
        </div>
    <?php } ?>
</div>
@endsection