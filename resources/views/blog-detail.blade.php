@extends('layout')
@section('content')
<div class="blog-content container mt-5 mb-5">
    <hr />
    <h1 class="text-center pt-3">{!!$blog['title']!!}</h1>
    {!!str_replace("/media/website/uploads/","/img/uploads/",$blog['body'])!!}
    <hr />
    <div class="blog-tags">
        <h4>
        @foreach($blogTags as $tags)
            <span class="badge badge-lg badge-primary">{{$tags}}</span>
        @endforeach
        </h4>
    </div>
</div>
@endsection
@section('custom_style')
<style>
.blog-content img{
    width:100%;
}
</style>
@endsection