@extends('layout')
@section('content')
<div class="container p-0 mt-5 mb-5" id="product-detail-page">
    <div class="row">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">HOME</a></li>
                    <li class="breadcrumb-item"><a href="#">{{$category['name']}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$product['title']}}</li>
                </ol>
            </nav>
        </div>
    </div>
    <br />
    <div class="row m-0">
        <div class="col-12 col-md-6 product-figure">
            <div class="product-images">
                @foreach ($product['images'] as $item)
                <img src="/img/product/{{$product['id']}}/{{$item['image']}}" class="product-img img-fluid"
                    onerror="this.onerror=null;this.src='/img/image-not-found.png';" />
                @endforeach
            </div>
        </div>
        <div class="col-12 col-md-6 p-0">
            <div class="product-info">
                <div class="product-info-inner">
                    <h3>{!!$product['title']!!}</h3>
                    @if($product['reference'] != null)
                    <h6>Refs: {!! $product['reference']['value'] !!}</h6>
                    @endif
                    <p>{!!$product['desc']!!}</p>
                    <br />
                    <br />
                    <div><b>RM {!!number_format($product['variant']['price'], 2)!!}</b></div>
                    <br />
                    <br />
                    <button class='btn btn-primary' data-toggle="modal" data-target="#appointment-modal">
                        SCHEDULE APPOINTMENT
                    </button>
                    <button class='btn btn-outline-primary' type="button" onclick="addSelection('{{$product['title']}}')">
                        ADD TO SELECTION
                    </button>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="row m-0">
        <div class="col-4 p-0">
            <a class="product-fast-action" href="/appointment">
                <i class="fas fa-map-pin"></i>
                <span>FIND A STORE</span>
            </a>
        </div>
        <div class="col-4 p-0">
            <button class="btn product-fast-action" data-toggle="modal" data-target="#appointment-modal">
                <i class="fas fa-phone-alt"></i>
                <span>CALL AN ADVISOR</span>
            </button>
        </div>
        <div class="col-4 p-0">
            <button class="btn product-fast-action" data-toggle="modal" data-target="#share-modal">
                <i class="fas fa-share"></i>
                <span>SHARE</span>
            </button>
        </div>
    </div>
    <hr />
    @if($product['specs'] != null)
    <div class="product-specs">
        @foreach($product['specs'] as $spec)
        <div class="product-spec">
            <div class="product-spec-label">{{$spec['label']}}</div>
            <div class="product-spec-value">{{$spec['value']}}</div>
        </div>
        @endforeach
    </div>
    @endif
</div>
<div id="appointment-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Make Appointment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="appointment-form" action="/appointment" method="post" data-validation='parsley'>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="">Salutation</label>
                            <select name="salutation" class="form-control">
                                <option value='Mr'>Mr</option>
                                <option value='Mrs'>Mrs</option>
                            </select>
                        </div>
                        <div class="form-group col">
                            <label for="">Name</label>
                            <input required name="name" class="form-control" placeholder="Enter your name">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="">Email</label>
                            <input required name="email" type="email" class="form-control"
                                placeholder="Enter your email">
                        </div>
                        <div class="form-group col">
                            <label for="">Mobile</label>
                            <input required name="mobile" class="form-control" placeholder="Enter your mobile">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea required name="message" class="form-control" rows="3"
                            placeholder="I would like to know about...">I would like to know about {{$product['title']}}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        <span class="btn-text">Send</span>
                        <span class="btn-spinner">
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Loading...
                        </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="share-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Share</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="share-form" action="/share" method="post" data-validation='parsley'>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="">Email</label>
                            <input required name="email" type="email" class="form-control"
                                placeholder="Enter your email">
                        </div>
                        <div class="form-group col">
                            <label for="">Name</label>
                            <input required name="name" class="form-control" placeholder="Enter your name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea required name="message" class="form-control" rows="3"
                            placeholder="I would like to know about...">I would like to share with you about {{$product['title']}}</textarea>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="">Recipient Email</label>
                            <input required name="recipient-email" type="email" class="form-control"
                                placeholder="Enter your email">
                        </div>
                        <div class="form-group col">
                            <label for="">Recipient Name</label>
                            <input required name="recipient-name" class="form-control" placeholder="Enter your name">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        <span class="btn-text">Share</span>
                        <span class="btn-spinner">
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Loading...
                        </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('custom_style')
<style>
    .slick-track {
        margin-left: 0;
    }

    .slick-prev-custom {
        left: 10px;
    }

    .slick-next-custom {
        right: 10px;
    }

    .slick-prev-custom,
    .slick-next-custom {
        position: absolute;
        top: 50%;
        display: block;
        width: 20px;
        height: 20px;
        padding: 0;
        -webkit-transform: translate(0, -50%);
        -ms-transform: translate(0, -50%);
        transform: translate(0, -50%);
        cursor: pointer;
        border: none;
        outline: none;
        background: transparent;
        z-index: 1;
        border-radius: 50%;
        font-size: 20px;
        color: rgba(0, 0, 0, 0.5);
    }
</style>
@endsection
@section('javascript')
<script>
    var addSelection = function(title){
        new Cart().add(title);
    }
    $(function () {
        $(".product-images").slick({
            slidesToShow: 1,
            prevArrow: '<i class="slick-prev-custom fas fa-chevron-circle-left"></i>',
            nextArrow: '<i class="slick-next-custom fas fa-chevron-circle-right"></i>',
        })
        $(".product-thumbnails img").click(function () {
            var $this = $(this);
            var index = $this.data('slick-index');

            $(".product-images").slick("slickGoTo", index);
        });

        $("#appointment-form").submit(function (e) {
            e.preventDefault();

            var $form = $(this);
            var $btn = $form.find('.btn-primary');
            var url = $form.attr('action');

            $("#appointment-form .btn-primary").addClass('btn-waiting').prop('disabled', true);
            $.ajax({
                type: "POST",
                url: url,
                data: $form.serialize(),
                success: function (response) {
                    var data = JSON.parse(response);
                    if (data.status == 'success') {
                        $("#appointment-form").trigger("reset");
                        $('#appointment-form').parsley().reset();
                        $("#appointment-form .btn-primary").removeClass('btn-waiting').prop(
                            'disabled', false);
                        $("#appointment-modal").modal("hide");
                        new MessageBox().success(
                            `Thank you for your message! <br/>We will get back to you soon.`
                            );
                    } else {
                        $("#appointment-form .btn-primary")
                            .removeClass('btn-waiting')
                            .prop('disabled', false);
                        $("#appointment-modal").modal("hide");
                        new MessageBox().fail();
                    }
                },
                error: function (request, status, error) {
                    $("#appointment-form .btn-primary")
                        .removeClass('btn-waiting')
                        .prop('disabled', false);
                    $("#appointment-modal").modal("hide");
                    new MessageBox().fail();
                },
            });
        });
    })
</script>
@endsection