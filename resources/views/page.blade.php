@extends('layout')
@section('before_wrapper')
@endsection
@section('content')
<div class="fullpage-slider-wrapper">
    <div class="slider-list">
        @foreach($page['banners'] as $banner)
        <div class="slider-item">
            <div class="slider-img slider-img-dekstop" style="background-image:url({{$banner['desktop_banner']['path'] . $banner['desktop_banner']['image']}});"></div>
            <div class="slider-img slider-img-mobile" style="background-image:url({{$banner['mobile_banner']['path'] . $banner['mobile_banner']['image']}});"></div>
            <div class="slider-content">
                <div class="slider-content-inner">
                    <div class='slider-tagline'>
                        Proposal Ring Collection
                    </div>
                    <div class='slider-subtagline'>
                        It just more than words.
                    </div>
                    <a href="{{$banner['desktop_banner']['link']}}" class="btn btn-primary btn-sm">Discover >></a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
@section('javascript')
<script>
$(function(){
    $('.focuspoint').focusPoint();
})
</script>
@endsection