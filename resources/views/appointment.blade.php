@extends('layout')
@section('content')
<div id="AppointmentPage">
  <div>
    @include('partial.page-header', [
    'pageTitle' => "Enquiry Us",
    'pageDesc' => "",
    ])
  </div>
  <div class="row">
    <div class="col">
      @include('partial.banner', ['imageUrl' => "/img/banner-default.jpg"])
    </div>
  </div>
  <div class="container mt-5 mb-5">
    <br />
    <div class="row">
      <div class="col-12 col-md-4">
        <div class="contact-box mb-4">
          <div class="contact-inner">
            <div class="contact-row">
              <div class="contact-title">
                Our Address
              </div>
              <div class="contact-desc">
                {{$config['contact']['addr']}} <br />
                {{$config['contact']['addr2']}} <br />
                {{$config['contact']['addr3']}}
              </div>
            </div>
            <div class="contact-row">
              <div class="contact-title">
                Phone
              </div>
              <div class="contact-desc">
                {{$config['contact']['mobile']}}
              </div>
            </div>
            <div class="contact-row">
              <div class="contact-title">
                Email
              </div>
              <div class="contact-desc">
                {{$config['contact']['email']}}
              </div>
            </div>
            <div class="contact-row">
              <div class="contact-title">
                Website
              </div>
              <div class="contact-desc">
                {{$config['contact']['website']}}
              </div>
            </div>
            <br />
            <div class="contact-row">
              <div class="contact-title">
                Appointment Preferred
              </div>
            </div>
            <div class="contact-row">
              <a class="contact-icon" target="_blank" href="{{$config['social']["pinterest"]}}"><i
                  class="fab fa-pinterest"></i></a>
              <a class="contact-icon" target="_blank" href="{{$config['social']["facebook"]}}"><i
                  class="fab fa-facebook-square"></i></a>
              <a class="contact-icon" target="_blank" href="{{$config['social']["instagram"]}}"><i
                  class="fab fa-instagram"></i></a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-8">
        <p>
          We’d love to hear from you
          Whether you have a question about pricing, advise, or anything else,
          our team is ready to answer all your questions
          we will get back to you soon!
        </p>
        <form id="appointment-form" action="/appointment" method="post" data-validation='parsley'>
          <div class="form-row">
            <div class="form-group col">
              <label for="">Salutation</label>
              <select name="salutation" class="form-control">
                <option value='Mr'>Mr</option>
                <option value='Mrs'>Mrs</option>
              </select>
            </div>
            <div class="form-group col">
              <label for="">Name</label>
              <input required name="name" class="form-control" placeholder="Enter your name">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col">
              <label for="">Email</label>
              <input required name="email" type="email" class="form-control" placeholder="Enter your email">
            </div>
            <div class="form-group col">
              <label for="">Mobile</label>
              <input required name="mobile" class="form-control" placeholder="Enter your mobile">
            </div>
          </div>
          <div class="form-group">
            <label>Message</label>
            <textarea required name="message" class="form-control" rows="3"
              placeholder="I would like to know about..."></textarea>
          </div>
          <input type="hidden" name="action" value="SendMail" />
          <button type="submit" class="btn btn-primary btn-send" href="#">
              <span class="btn-text">Send</span>
              <span class="btn-spinner">
                  <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                  Loading...
              </span>
          </button>
          <br>
        </form>
      </div>
    </div>

    <div class="">
      <iframe class="" src="{!!$config['contact']['location']!!}" width="100%" height="300px" frameborder="0"
        style="border:0;" allowfullscreen=""></iframe>
    </div>
  </div>
</div>

@endsection
@section('javascript')
<script>
  $(function () {
    var message = new Cart().getMessage();
    if(message){
      $("textarea[name='message']").val(`I would interested in this few items ${message}.`);
    }

    $("#appointment-form").submit(function (e) {
      e.preventDefault();

      var $form = $(this);
      var $btn = $form.find('.btn-primary');
      var url = $form.attr('action');

      $("#appointment-form .btn-primary").addClass('btn-waiting').prop('disabled', true);

      $.ajax({
        type: "POST",
        url: url,
        data: $form.serialize(),
        success: function (response) {
          var data = JSON.parse(response);

          if (data.status == 'success') {
            $("#appointment-form").trigger("reset");
            $('#appointment-form').parsley().reset();
            $("#appointment-form .btn-primary").removeClass('btn-waiting').prop('disabled', false);
            $("#appointment-modal").modal("hide");
            new MessageBox().success(`Thank you for your message ! <br/>We will get back to you soon.`);
            new Cart().reset();
          } else {
            $("#appointment-form .btn-primary").removeClass('btn-waiting').prop('disabled', false);
            $("#appointment-modal").modal("hide");
            new MessageBox().fail();
          }
        },
        error: function (request, status, error) {
          $("#appointment-form .btn-primary").removeClass('btn-waiting').prop('disabled', false);
          $("#appointment-modal").modal("hide");
          new MessageBox().fail();
        },
      });
    });
  });
</script>
@endsection