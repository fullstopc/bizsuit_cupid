@extends('layout')
@section('content')
<br />
<div>
  @include('partial.page-header', [
      'pageTitle' => $page['title'],
      'pageDesc' => $page['desc'],
  ])
</div>
  <div class="row">
    <div class="col">
    @if($imageUrl != null)
        @include('partial.banner', ['imageUrl' => $imageUrl ])
    @endif
    </div>
  </div>
<div class="container">
{!!$page['body']!!}
</div>
@endsection