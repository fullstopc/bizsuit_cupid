@extends('layout')
@section('content')
<div id="GalleryPage">
    <div>
    @include('partial.page-header', [
      'pageTitle' => $page['title'],
      'pageDesc' => $page['desc'],
    ])
    </div>
    <div class="container">
        <div class="gallery-list">
            @foreach($page['images'] as $image)
                <div class="gallery-item ">
                    <div><img class="potrait" src="{{ $image['path']. $image['image'] }}"/></div>
                    <div>
                        <h5>{{$image['title']}}</h5>
                        <p>
                            {{$image['body']}}
                        </p>
                        <div class="gallery-link">
                            <a href='{{$image['link']}}'>View More</a>
                        </div>
                    </div>  
                </div>
            @endforeach
        </div> 
    </div>
</div>
@endsection
@section('custom_style')
<style>

</style>
@endsection