@extends('layout')
@section('custom_style')

@endsection
@section('content')
<div>
  @include('partial.page-header', [
      'pageTitle' => "The page are not available for now",
      'pageDesc' => "
      At Cupid jewellery, you can based on your own inspiration or his/her favorite jewelry design, <br />
      custom made a unique piece that belongs only to your loved one. <br />
      <br />
      From then, your jewelry design will be carried out from our finest jewelry craftsmen, <br />
      to ensure a flawless presentation of a masterpiece which as unique as your loved one.<br />
      ",
  ])
</div>
<div class="row">
  <div class="col">
    @include('partial.banner', ['imageUrl' => "/img/category/bespoke_subpage_banner.jpg"])
  </div>
</div>
@endsection