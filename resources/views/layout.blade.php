<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name='keywords' content='' />

    <title>Cupid Jewellery</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/app.css" >
    
    @yield("custom_style")
</head>

<body class="">
    @yield('before_wrapper')
    <div class="wrapper" id="wrapper">
        @include("partial.header")

        <section id="content">
            @yield('content')
        </section>
        
        @include("partial.footer")
    </div>
    
    <div class="modal" tabindex="-1" role="dialog" id="message-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    @yield('after_wrapper')
    @include("partial.script")
    @yield('javascript')
</body>
</html>
