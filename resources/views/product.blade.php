@extends('layout')
@section('custom_style')
<style>
    .product-list-container .page-sort-control{
        width:180px;
    }
    .product-list-container .page-sort-label{
        padding-right:5px;
    }
    .product-list-empty{
        display: flex;
        align-items: center;
        justify-content: center;
        width: 96%;
        padding: 10px 10px;
        border: 1px solid #202e5f;
        font-family: "Cinzel", san-serif;
        color: #202e5f;
        margin: 2%;
    }
</style>
@endsection
@section('content')
    <br />
    <div>
        @include('partial.page-header', [
            'pageTitle' => $category['name'],
            'pageDesc' => $category['desc'] ?? "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consectetur neque quis cursus semper. Pellentesque tincidunt laoreet hendrerit. Aenean at consectetur purus. Aenean efficitur tristique urna vel pharetra.",
        ])
    </div>
    <div class="row">
        <div class="col">
            @if(array_key_exists('banner',$category) && !empty($category['banner']['image']))
                @include('partial.banner', ['imageUrl' => "/img/category/{$category['id']}/" . $category['banner']['image']])
            @else
                @include('partial.banner', ['imageUrl' => "/img/banner-product.jpg"])
            @endif
        </div>
    </div>
    <?php if ($pagination["total"] == 0) { ?>
    <div class="container">
        <div class="product-list-empty">
            No Data
        </div>
    </div>
    <?php } ?>
    <?php if ($pagination["total"] > 0) { ?>
        <div class='container product-list-container'>
        <div class="row">
            <div class="col">
                <div class="d-flex align-items-center justify-content-end mt-2 mb-2">
                    <span class='page-sort-label'>Sort By:</span>
                    <select class='form-control form-control-sm page-sort-control' onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                        <?php foreach($sorting["links"] as $item) { ?>
                            <option value="{!!$item['url']!!}" <?php echo $sorting["current"] == $item["code"] ? "selected" : "" ?>>{{$item["label"]}}</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class='row'>
        <?php foreach($category['products'] as $product) { ?>
        <div class="col-md-4 col-sm-6 col-12">
            <a href="/collections/{{$category['code']}}/{{$product['url']}}" style="text-decoration:none; color:#000">
                <div class='product-item'>
                    <div class="product-item-inner">
                        <div class="product-img">
                            <img src="/storage/product/{{$product['id']}}/{{$product['images'][0]['image']}}" />
                            <!-- <img src="/img/product/{{$product['id']}}/{{$product['images'][0]['image']}}" class="product-img img-fluid" onerror="this.onerror=null;this.src='/img/image-not-found.png';" /> -->
                            <!-- <div class="product-overlay"></div> -->
                        </div>
                        <div class='product-name'>{{$product['title']}}</div>
                        <div class='product-price'>
                            RM {{ number_format($product['variant']['price'],2) }}
                        </div>
                        <div class='product-tools'>
                            <div href="/collections/{{$category['code']}}/{{$product['url']}}"
                                class='product-button btn btn-sm btn-outline-primary rounded-0'>
                                Discover
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <?php } ?>
        </div>
        <div class='row'>
            <div class="col">
                <div class="d-flex align-items-center justify-content-end mt-2 mb-2">
                    <nav>
                        <ul class="pagination">
                            <li class="page-item {{$pagination["current_page"] == 1 ? "disabled" : "" }}"><a class="page-link" href="{{$pagination["prev_page_url"]}}">Previous</a></li>
                            <?php foreach($pagination["links"] as $index => $link) { ?>
                            <li class="page-item {{$pagination["current_page"] == ($index + 1) ? "active" : "" }}"><a class="page-link" href="{{$pagination["current_page"] == ($index + 1) ? "#" : $link }}">{{$index + 1}}</a></li>
                            <?php } ?>
                            <li class="page-item {{$pagination["current_page"] == $pagination["last_page"] ? "disabled" : "" }}"><a class="page-link" href="{{$pagination["next_page_url"]}}">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
</div>
    <?php } ?>
@endsection