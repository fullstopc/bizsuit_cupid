@extends('layout')
@section('content')
<style>
</style>
<div>
  @include('partial.page-header', [
      'pageTitle' => "TESTIMONIAL",
      'pageDesc' => "With the share of their happiness.",
  ])
</div>
<div class="row">
  <div class="col">
    @include('partial.banner', ['imageUrl' => "/img/banner-default.jpg"])
  </div>
</div>
<div class='container pt-4'>
    <div class="row">
        <div class="col-sm-8 col-12 my-3 p-3 border mx-auto">
            <img class="img-fluid" src="/img/logo.png" alt="{{$config['company_name']}}" />
            <p class="px-sm-5 px-3">{{$config['contact']['addr']}} {{$config['contact']['addr2']}} {{$config['contact']['addr3']}}</p>
            <div class="px-sm-5 px-3"><h1 class="text-secondary">5.0 <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></h1><p>69 reviews</p></div>
        </div>
    </div>
    
    <?php /* if (count($testimonials) == 0) { ?>
    <div class="row">
        <div class="col">
            <div class="d-flex align-items-center justify-content-center p-4 border">
                <span>No Data</span>
            </div>
        </div>
    </div>
    <?php } */?>
    <div class="review-list py-5">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4">
                <div class="review-box">
                    <h5>Sephine Sum</h5>
                    <p class="text-muted mb-2">Dec 2019</p>
                    <div class="text-secondary pb-3"><small><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></small></div>
                    <div class="readmore link-unstyle">
                        <p class="block-with-text-hide">
                            The service at Cupid Jewellery was outstanding. 
                            Gin and his wife greeted us warmly the moment we stepped into the shop.
                            As both my fiancé and I have no idea what we want as our wedding rings, Gin patiently shown us their signature designs and even took out a proposal ring for me to match with their wedding rings as I forgot to wear my proposal ring that day. 
                            He was extremely helpful in giving us advice to choose our wedding rings. 
                            Thank you, we love our rings so much! Highly recommended!
                        </p>
                        <p class="block-with-text">
                            The service at Cupid Jewellery was outstanding. 
                            Gin and his wife greeted us warmly the moment we stepped into the shop. 
                            As both my fiancé and I have no idea what we want as our wedding rings, Gin... 
                        </p>
                        <a class="expand">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="review-box">
                    <h5>tingting lim</h5>
                    <p class="text-muted mb-2">Nov 2019</p>
                    <div class="text-secondary pb-3"><small><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></small></div>
                    <div class="readmore link-unstyle">
                        <p class="block-with-text-hide">
                            💯satisfied with the pair of rings that we had bought at cupid jewellery.  
                            Gin and his wife has been very helpful from choosing, short listing to finalizing process of our wedding bands.
                            We are greatly appreciated with their kind assistance. 
                            Not to mention the effort they actually made a trip in to Singapore just to pass us our band for our photo shoot.
                            Thank you so much!!! Without the both of you  . We will have a harder time on decision 
                        </p>
                        <p class="block-with-text">
                            💯satisfied with the pair of rings that we had bought at cupid jewellery.  
                            Gin and his wife has been very helpful from choosing, short listing to finalizing process of our wedding bands.
                            We are...
                        </p>
                        <a class="expand">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="review-box">
                    <h5>Daniel Chong</h5>
                    <p class="text-muted mb-2">Dec 2019</p>
                    <div class="text-secondary pb-3"><small><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></small></div>
                    <div class="readmore link-unstyle">
                        <p class="block-with-text-hide">
                            Great experience, excellent service.
                            The propose ring was amazing, my fiancée love it!
                            Thanks to Mr. Gin and his Wife to help me choose the perfect ring and make it happen even before scheduled.
                            Complete satisfaction with my purchase is the best description.
                            Kudos to Mr. Gin and his Wife.
                        </p>
                        <p class="block-with-text">
                            Great experience, excellent service.
                            The propose ring was amazing, my fiancée love it!
                            Thanks to Mr. Gin and his Wife to help me choose the perfect ring and make it happen even before scheduled.
                            Complete satisfaction...
                        </p>
                        <a class="expand">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="review-box">
                    <h5>Dominick Ong</h5>
                    <p class="text-muted mb-2">Dec 2019</p>
                    <div class="text-secondary pb-3"><small><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></small></div>
                    <div class="readmore link-unstyle">
                        <p class="block-with-text-hide">
                            If you still looking around to get a masterpiece for your proposal, visit Cupid! I did research quite a numbers of jewellery shop before i found Cupid, all couldnt satisfy me. 
                            I found Cupid is knowledgeable and professional on what they are doing. 
                            So do not hesitate and just visit the shop with any design template come in your mind, they can fulfill your imagination! Highly recommend!
                        </p>
                        <p class="block-with-text">
                            If you still looking around to get a masterpiece for your proposal, visit Cupid! I did research quite a numbers of jewellery shop before i found Cupid, all couldnt satisfy me.
                            I found Cupid is...
                        </p>
                        <a class="expand">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="review-box">
                    <h5>Zhenni Yang</h5>
                    <p class="text-muted mb-2">Dec 2019</p>
                    <div class="text-secondary pb-3"><small><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></small></div>
                    <div class="readmore link-unstyle">
                        <p class="block-with-text-hide">
                            Gin and Xiao Li are very nice and patient person. 
                            They tried their best to figure out what kind of ring we want to customize. 
                            Still remember when I first met Gin, he told me “I know what you want and I am confident we can do it.” 
                            Finally the rings came out and it is EXACTLY WHAT WE WANTED! 
                            Thanks Gin & Xiao Li for their works and me and my bf love the weeding bands! 
                            Will definitely recommend this shop! Five star!
                        </p>
                        <p class="block-with-text">
                            Gin and Xiao Li are very nice and patient person. 
                            They tried their best to figure out what kind of ring we want to customize.
                            Still remember when I first met Gin, he told me “I know what you want...
                        </p>
                        <a class="expand">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="review-box">
                    <h5>Beryl Chai</h5>
                    <p class="text-muted mb-2">Nov 2019</p>
                    <div class="text-secondary pb-3"><small><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></small></div>
                    <div class="readmore link-unstyle">
                        <p class="block-with-text-hide">
                            We are truly impressed with the appointment basis service provided by Gin and Xiaoli, three hours were discussing from the designs till the laser names for our wedding band. 
                            it also turns out the same as the 4D drawing. 
                            We love it very much can't wait to wear it every single day in two months. 😍
                        </p>
                        <p class="block-with-text">
                            We are truly impressed with the appointment basis service provided by Gin and Xiaoli, three hours were discussing from the designs till the laser names for our wedding band. 
                            it also turns out...
                        </p>
                        <a class="expand">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="review-box">
                    <h5>Yilin K</h5>
                    <p class="text-muted mb-2">Nov 2019</p>
                    <div class="text-secondary pb-3"><small><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></small></div>
                    <div class="readmore link-unstyle">
                        <p class="block-with-text-hide">
                            Excellent services from Xiao Li and Gin. 
                            Both of them have talked to us very patiently about the variety of designs and helped us in getting the best rings that suited my fiancé and I. 
                            I specifically ask for certain material and size and super glad they able to make it in time!!
                            My fiancé and I are very pleased with our rings and would totally recommend their excellent services and products.
                        </p>
                        <p class="block-with-text">
                            Excellent services from Xiao Li and Gin. 
                            Both of them have talked to us very patiently about the variety of designs and helped us in getting the best rings that suited my fiancé and I.
                            I specifically ask...
                        </p>
                        <a class="expand">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="review-box">
                    <h5>lim yingshen</h5>
                    <p class="text-muted mb-2">Aug 2019</p>
                    <div class="text-secondary pb-3"><small><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></small></div>
                    <div class="readmore link-unstyle">
                        <p class="block-with-text-hide">
                            I having a great experience when purchase my proposal ring and wedding band at this shop.
                            The boss not only push the ring to us, but also teach us how to see and check the diamond, and most important is the reasonable pricing for all Jewellery!!
                            Recommended for all of my friend who wish to propose or wedding!!
                        </p>
                        <p class="block-with-text">
                            I having a great experience when purchase my proposal ring and wedding band at this shop.
                            The boss not only push the ring to us, but also teach us how to see and check the diamond, and most...
                        </p>
                        <a class="expand">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="review-box">
                    <h5>Khartic Rao Manokaran</h5>
                    <p class="text-muted mb-2">Sept 2019</p>
                    <div class="text-secondary pb-3"><small><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></small></div>
                    <div class="readmore link-unstyle">
                        <p class="block-with-text-hide">
                            Xiao Li & partner are the most accommodating people i have ever come across. 
                            They assisted us well in making the right selection for our wedding band. 
                            We had our own custom designs and they deliberately understood the detailing so perfectly and made our dream come true. 
                            Excellent workmanship and best customer service ever!!! 
                            Thanks a million both of you for actualising our far possible dream into beautiful reality...
                        </p>
                        <p class="block-with-text">
                            Xiao Li & partner are the most accommodating people i have ever come across. 
                            They assisted us well in making the right selection for our wedding band. 
                            We had our own custom designs and they...
                        </p>
                        <a class="expand">Read More</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-5">
            <div class="col-12">
                <a href='https://www.google.com/search?q=google+review+cupid+jewellery&rlz=1C1CHBF_enMY878MY878&oq=google+review+&aqs=chrome.1.69i59l3j0l2j69i60l3.3594j0j4&sourceid=chrome&ie=UTF-8#lrd=0x31da6c1121ac3099:0x6496db41ba7fa866,1,,,' target="_blank" class="btn btn-primary btn-lg">More Review</a>
                <a href="/appointment" class="btn btn-outline-primary btn-lg">Make Appointment</a>
            </div>
        </div>
    </div>
    
    <?php if (count($testimonials) > 0) { ?>
        <div class="blog-list">
            @foreach($testimonials as $testimonial)
                <div class="blog-item">
                    <div class="blog-content">
                        <a class="blog-title" href='/education/{{$blog['url']}}'>{!!$testimonial["title"]!!}</a>
                        <div class="blog-desc">{!!$testimonial["desc"]!!}</div>
                    </div>
                </div>
            @endforeach
        </div>
    <?php } ?>
</div>
@endsection

@section('javascript')
<script>
    $(function() {
  $('.readmore').click(function() {
    $(this).find('.block-with-text-hide').toggleClass('d-block');
    $(this).find('.block-with-text').toggleClass('d-none');
  });
});
    
</script>
@endsection