@extends('layout')
@section('content')
<div>
  @include('partial.page-header', [
      'pageTitle' => "The Making of a Harry Winston Engagement Ring",
      'pageDesc' => "",
  ])
</div>
<div class="row">
  <div class="col">
    @include('partial.banner', ['imageUrl' => "/img/banner-default.jpg"])
  </div>
</div>
<div class="container mt-5 mb-5" id="BlockContent">
  <div class="text-center">
    <!-- <hr class="">
    <p class=""> </p>
    <div class="arrow-container">
      <a href="#" class="">
        <img src="/img/fleche-jewcollec-down.png" class="arrow-bottom" alt="Scroll to content">
      </a>
    </div> -->
  </div>
  <div class="" id="">
    <blockquote class="text-center blockquote">
      <p>“The making of a Harry Winston engagement ring, from the selection of the perfect diamond to its flawless
        presentation”</p>
    </blockquote>
    <br/><br/>
    <div class="row">
      <div class="col">
        <p class="title-from-us-to-yours">Mr. Winston once said :</p>
        <blockquote>
          <p class="h4">“Each diamond has a different nature. Each diamond must be handled the way you handle a
            person.”</p>
        </blockquote>
      </div>
      <p class="col">Since the House was founded in 1932, the Harry Winston master craftsman has been committed to
        creating the most exquisite jewels that celebrate and enhance the natural beauty of each individual diamond.
        &nbsp;This enduring commitment guides the creation of all Harry Winston jewels, and the philosophy behind
        the artistry of a Harry Winston engagement ring. &nbsp;To perfectly highlight a diamond’s true brilliance
        requires the culmination of efforts between true fine jewelry experts, beginning with the selection of a
        single stone until it is placed in its own impeccable setting.</p>
    </div>
    <div class="row">
      <div class="col"><img alt="An extraordinary Classic Winston Emerald-cut Engagement Ring."
          src="/img/fromourhands_1.png" style="height:204px; width:450px"></div>
      <div class="col">
        <h2>The Four Cs</h2>
        <p>The Four Cs refer to carat weight, clarity, color, and cut. The science behind the sparkle, these
          guidelines provide solid numbers to help quantify a stone’s quality using charts and scales that are
          standardized for all gemstones. Carats provide a precise numeration of a diamond’s weight, and from there
          the precise value of a diamond is further established by assessing its clarity and color, and the
          precision of its cut. For the master craftsmen at Harry Winston, these figures form the basis for a more
          complex calculus that will determine the selection and setting of your diamond engagement ring.</p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <h2>Diamond Selection</h2>
        <p>A Harry Winston diamond is selected for its impeccable quality and character. Completely free from any
          visible inclusions, each diamond is meticulously cut to reveal its innate brilliance and radiant fire. The
          precision of a diamond’s cut is what will determine its final size, shape and facets, which all combine to
          enhance a diamond’s overall beauty. In a well-cut diamond, the facets are subtly angled, symmetrical and
          balanced, ensuring that light refracts through the diamond in a manner that will optimize and enhance its
          overall sparkle. Equally important is creating an appealing and balanced shape, where the overall surface
          of the stone is as intriguing as its luminous, shifting depths.</p>
      </div>
      <div class="col"><img alt="A black and white design sketch of the Classic Winston Emerald-Cut Engagement Ring."
          src="/img/fromourhands_2.png" style="height:382px; width:500px"></div>
    </div>
    <div class="row">
      <div class="col"><img alt="A sketch of various angles of the Classic Winston Emerald-Cut Engagement Ring."
          class="adaptative-img" src="/img/fromourhands_3.png" style="height:354px; width:404px">
      </div>
      <div class="col">
        <h2>Design</h2>
        <p>Harry Winston’s approach has always been to design around the individual beauty of individual diamonds by
          creating the perfect setting for each stone to shine. Each creation begins with a sketch done by hand in
          meticulous detail. This allows the designer to convey to the craftsmen the fluidity and overall harmony of
          the design that brings out the collective beauty of both the setting and the stone.</p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <br>
        <h2>Setting</h2>
        <p>In creating its fine jewelry designs, the House of Harry Winston has been a pioneer in strictly utilizing
          platinum settings that allow diamonds to appear as if they are floating in their settings. The most
          durable precious metal, platinum possesses an ethereal, light color that compliments rather than competes
          with a diamond’s colorless nature, ensuring that the beauty of the diamond stands at the forefront of each
          design. Mr. Winston firmly believed that nothing should distract from a diamond's natural brilliance,
          leading to innovative techniques in craftsmanship, such as the House’s iconic split-prong settings. The
          setting securely holds the diamond in place, with eight ultra-fine prongs that seamlessly blend into the
          ring’s design, seeming to disappear behind the radiance of the center stone.</p>
      </div>
      <div class="col"><img alt="Setting of the Classic Winston Emerald-Cut Engagement Ring. "
          src="/img/fromourhands_4.png" style="height:428px; width:500px"></div>
      <div class="clearfix">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col">
        <h2>Polishing</h2>
        <p>In creating a Harry Winston jewel, what is unseen to the eye is equally as important as the surface
          presentation. After master craftsmen have placed the diamond, the platinum settings – including those
          beneath the diamond – are all re-polished to ensure that the luminance of each diamond shines through.</p>
      </div>
      <div class="col">
        <h2>Final</h2>
        <p>After each detail is meticulously reviewed, the finished jewel leaves the workshop. The culmination of
          efforts and skills between gemologist, designer and craftsman, each engagement ring represents the high
          quality standards that are synonymous with the Harry Winston name, and serves as a precious symbol as rare
          and unique as true love itself.</p>
      </div>
    </div>
    </div>
  </div>
</div>
@endsection
@section('custom_style')
<style>
#BlockContent p{
  margin: 20px 0px;
}
</style>
@endsection