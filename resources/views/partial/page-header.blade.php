<div>
    <h3 class="page-title text-center pl-5 pr-5">{{$pageTitle}}</h3>
    @if(isset($pageDesc) && $pageDesc != "")
    <p class="page-desc text-center">
        {!!$pageDesc!!}
    </p>
    @endif
</div>