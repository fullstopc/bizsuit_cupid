<header id="header">
    <div class="header-inner">
        <div class="header-inner-top">
            <div class="header-logo">
            <a class='' href='/'><img class="img-fluid" src="/img/logo.png" alt="{{$config['company_name']}}" /></a>
            </div>
            <div class="header-nav">
                <ul class="nav-menu nav-one">
                    <li class="nav-item active">
                        <a class="nav-link" href="#" data-level="1">THE BRAND</a>
                        <ul class="nav-menu nav-two">
                            <li><a class="nav-link" href="/our-story" data-level="1.1" data-page="our-story">OUR STORY</a></li>
                            <li><a class="nav-link" href="/our-diamond" data-level="1.2" data-page="our-diamond">OUR DIAMOND</a></li>
                            <li><a class="nav-link" href="/the-one" data-level="1.3" data-page="the-one">THE ONE</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/collections/engagement-rings" data-page="collections/engagement-rings">ENGAGEMENT RING</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="/collections/wedding-band" data-page="collections/wedding-band">WEDDING BAND</a></li>
                    <li class="nav-item">
                        <a class="nav-link" href="/collections/jewelry" data-page="collections/jewelry" data-level="4">JEWELRY</a>
                        <ul class="nav-menu nav-two">
                            <li><a class="nav-link" href="/collections/prince-collection" data-level="4.1" data-page="collections/prince-collection">PRINCESS COLLECTION</a></li>
                            <li><a class="nav-link" href="/collections/necklace" data-level="4.2" data-page="collections/necklace">NECKLACES</a></li>
                            <li><a class="nav-link" href="/collections/earings" data-level="4.3" data-page="collections/earings">EARINGS</a></li>
                        </ul>
                    </li>                     
                    <li class="nav-item"><a class="nav-link" href="/bespoke" data-page="bespoke">BESPOKE</a></li>
                </ul>
            </div>
            <div class="toolbar-list">
                <a class="toolbar-item toolbar-search" target="_blank" href="tel:{{str_replace(" ","", $config['contact']["mobile"])}}"><i class="fas fa-phone-square"></i></a>
                <a class="toolbar-item social-fb" target="_blank" href="{{$config['social']["facebook"]}}"><i class="fab fa-facebook-square"></i></a>
                <a class="toolbar-item social-ins" target="_blank" href="{{$config['social']["instagram"]}}"><i class="fab fa-instagram"></i></a>
            </div>
            <div class="header-hamburger">
                <div class="hamburger-nav">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
        
        <div class="header-mobile-nav">
            <div class="header-mobile-nav-close">
                <span class="material-icons">close</span>
            </div>
            <ul class="nav-menu nav-one">
                <li class="nav-item active">
                    <a class="nav-link" href="#" data-level="1">
                        <span class="nav-text">THE BRAND</span>
                        <span class="nav-icon">
                            <span class="nav-plus material-icons">keyboard_arrow_right</span>
                            <span class="nav-minus material-icons">keyboard_arrow_down</span>
                        </span>
                    </a>
                    <ul class="nav-menu nav-two">
                        <li><a class="nav-link" href="/our-story" data-level="1.1" data-page="our-story">Our Story</a></li>
                        <li><a class="nav-link" href="/our-diamond" data-level="1.2" data-page="our-diamond">Our Diamond</a></li>
                        <li><a class="nav-link" href="/the-one" data-level="1.3" data-page="the-one">THE ONE</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/collections/engagement-rings" data-page="collections/engagement-rings">ENGAGEMENT RING</a>
                </li>
                <li class="nav-item"><a class="nav-link" href="/collections/wedding-band" data-page="collections/wedding-band">WEDDING BAND</a></li>
                <li class="nav-item">
                    <a class="nav-link" href="/collections/jewelry" data-page="collections/jewelry" data-level="4">
                        <span class="nav-text">JEWELRY</span>
                        <span class="nav-icon">
                            <span class="nav-plus material-icons">keyboard_arrow_right</span>
                            <span class="nav-minus material-icons">keyboard_arrow_down</span>
                        </span>
                    </a>
                    <ul class="nav-menu nav-two">
                        <li><a class="nav-link" href="/collections/prince-collection" data-level="4.1" data-page="collections/prince-collection">PRINCESS COLLECTION</a></li>
                        <li><a class="nav-link" href="/collections/necklace" data-level="4.2" data-page="collections/necklace">NECKLACES</a></li>
                        <li><a class="nav-link" href="/collections/earings" data-level="4.3" data-page="collections/earings">EARINGS</a></li>
                    </ul>
                </li>
                
                <li class="nav-item"><a class="nav-link" href="/bespoke" data-page="bespoke">BESPOKE</a></li>
                <li class="nav-item"><a class="nav-link" href="/education" data-page="/education">EDUCATION</a></li>
                <li class="nav-item"><a class="nav-link" href="/testimonial" data-page="/testimonial">TESTIMONIAL</a></li>
                <li class="nav-item"><a class="nav-link" href="/appointment" data-page="/appointment">APPOINTMENT</a></li>
                <li class="nav-item"><a class="nav-link" href="/gallery" data-page="/gallery">GALLERY</a></li>
            </ul>
            <div class="toolbar-list">
                <a class="toolbar-item toolbar-search"><i class="fas fa-phone-square"></i></a>
                <a class="toolbar-item social-fb"><i class="fab fa-facebook-square"></i></a>
                <a class="toolbar-item social-ins"><i class="fab fa-instagram"></i></a>
            </div>
        </div>
    </div>
</header>