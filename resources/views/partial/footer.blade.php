<footer id="footer">
    <div class='footer-inner'>
        <div class="footer-left">
            <ul class="footer-navbar">
                <li class=""><a class="nav-link" href="/education">EDUCATION</a></li>
                <li class=""><a class="nav-link" href="/testimonial">TESTIMONIAL</a></li>
            </ul>
        </div>
        <div class="footer-middle">
            <a href='/'><img src='/img/logo-icon.png'/></a>
        </div>
        <div class="footer-right">
            <ul class="footer-navbar">
                <li class=""><a class="nav-link" href="/appointment">APPOINTMENT</a></li>
                <li class=""><a class="nav-link" href="/gallery">GALLERY</a></li>
            </ul>
        </div>
    </div>
    <div class="footer-copyright">
        © 2019 Cupid Jewellery
    </div>
</footer>