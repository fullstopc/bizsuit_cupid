<script src="/assets/js/app.js"></script>
<script>
var initMobileNav = function(){
    $(".hamburger-nav").click(function(){
        $("body").toggleClass("hamburger-active")
    });
    $(".header-mobile-nav .nav-one > li").click(function(){
        $(this).toggleClass("active");
    });
    $(".header-mobile-nav .nav-icon").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        
        $(this).parent().toggleClass("nav-open");
    });
    $("#header .header-mobile-nav-close").click(function(){
        $("body").removeClass("hamburger-active")
    });
}
var initPageChange = function(){
    var pathname = window.location.pathname || 'home';
    if(pathname == '/') pathname = 'home';

    $("#header .header-nav .nav-link").removeClass("active");
    $("#header .header-nav .nav-link").each(function(){
        var page = $(this).data("page");
        var level = $(this).data("level");
        var active = pathname.includes(page);

        if(active){
            $(this).addClass("active");
            
            if(level){
                $(`[data-level='${(level + "")[0]}']`).addClass("active")
            }
        }
    });
}
var MessageBox = function(){
    this.selector = '#message-modal';
    this.header = this.selector + ' .modal-title';
    this.body = this.selector + ' .modal-body';
    
    this.success = function(msg){
        $(this.header).html('Success');
        $(this.body)
            .addClass('text-center')
            .html(msg);
        $(this.selector).modal('show');
    }
    this.fail = function(){
        $(this.header).html('Fail');
        $(this.body).html('Something wrong happen.');
        $(this.selector).modal('show');
    }
};

var Cart = function() {
    this.cookie = null;
    this.items = [];

    this.init = function(){
        this.cookie = this.getCookie('selection');
        if(!this.cookie){
            this.setCookie('selection',JSON.stringify([]), 3);
            this.cookie = this.getCookie('selection');
        }
        this.items = JSON.parse(this.cookie);
    }

    this.add = function(name){
        new MessageBox().success(`
        this item had add into selection. You may go to <a href='/appointment'>here</a> to make appointment.
        `);

        this.init();
        let isExists = this.items.includes(name);
        if(isExists)
            return;

        this.items.push(name);
        this.setCookie('selection',JSON.stringify(this.items), 3);
    }

    this.getMessage = function(){
        this.init();
        return this.items.join(",");
    }

    this.reset = function(){
        this.setCookie('selection',JSON.stringify([]), 3);
    }

    this.setCookie = function(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    this.getCookie = function(cname){
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
            c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}

$(function(){
    initMobileNav();
    initPageChange();
    cbpBGSlideshow.init();

    $("form[data-validation='parsley']").parsley({
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid', // Comment this option if you don't want the field to become green when valid. Recommended in Google material design to prevent too many hints for user experience. Only report when a field is wrong.
        errorsWrapper: '<div class="text-danger"></div>',
        errorTemplate: '<span></span>',
        trigger: 'change'
    })

    new Cart().init();
});
</script>