window.$ = window.jQuery = require('jquery');

require('parsleyjs');
require('bootstrap');
require('slick-carousel');
require('imagesloaded');
require('jquery-focuspoint');
require('./vendor/fullscreen-slider');