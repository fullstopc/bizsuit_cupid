<?php 
return [
    'app_url' => '',
    'company_name' => 'Cupid Jewellery',
    'gakey' => '',
    'gtkey' => '',
    'contact' => [
        'website' => 'www.cupidjewellery.com',
        'office' => '',
        'mobile' => '6018 578 4639',
        'addr' => 'No 15-A, Jalan Dedap 8,',
        'addr2' => 'Taman Johor Jaya,',
        'addr3' => '81100 Johor Bahru, Johor',
        'email' => 'service@cupidjewellery.com',
        'location' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d997.094188909766!2d103.80498482920991!3d1.540249999929801!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da6c1121ac3099%3A0x6496db41ba7fa866!2sCupid%20Jewellery!5e0!3m2!1sen!2smy!4v1580792252297!5m2!1sen!2smy',
        'workhour' => '10AM ~ 6PM, All Day (except Public Holidays)'
    ],
    'social' => [
        'facebook' => 'https://www.facebook.com/theCupidJewellery/',
        'instagram' => 'https://www.instagram.com/cupidjewellery/',
        'pinterest' => '',
    ],
    'mail' => [
        'from' => 'debug@bizsuit.com.my',
        'to' => 'shchong1991@gmail.com',
        'host' => 'smtp.gmail.com',
        'port' => '587',
        'username' => '',
        'password' => '',
    ],
];