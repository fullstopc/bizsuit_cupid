<?php

use Philo\Blade\Blade;
use Tightenco\Collect;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class HelperController
{    
    protected $config;
    protected $path;

    function __construct($config, $path)
    {
        $this->config = $config;
        $this->path = $path;
    }
   
    function ImportData($query, $body)
    {
        $data = $body;
        $directoryName = $this->path['data'];
        if(!is_dir($directoryName)){
            mkdir($directoryName, 0755, true);
        }

        foreach($data as $key => $item)
        {
            $filename = "$key.json";
            $file = $directoryName.$filename;
            if(!is_file($file)){
                file_put_contents($file, '');
            }
            file_put_contents($file, json_encode($item));
        }
    }

    function ImportImages($query, $body)
    {
        $data = $body;
        $directories = [
            './img/content/', './img/product/', './img/uploads/'
        ];

        // create all directory
        foreach($directories as $directory)
        {
            if(!is_dir($directory)){
                mkdir($directory, 0755, true);
            }
        }

        foreach($data['products'] as $image)
        {
            $id = $image['id'];
            $name = $image['name'];
            $url = $image['url'];

            $directory = "./img/product/$id/";
            if(!is_dir($directory)){
                mkdir($directory, 0755, true);
            }

            copy($url, $directory.$name);
        }

        foreach($data['contents'] as $image)
        {
            $id = $image['id'];
            $name = $image['name'];
            $url = $image['url'];

            $directory = "./img/content/$id/";
            if(!is_dir($directory)){
                mkdir($directory, 0755, true);
            }

            copy($url, $directory.$name);
        }

        foreach($data['uploads'] as $image)
        {
            $name = $image['name'];

            $directory = "./img/uploads/";
            if(!is_dir($directory)){
                mkdir($directory, 0755, true);
            }
            copy($url, $directory.$name);
        }
    }

    function ImportZip($query, $body)
    {
        $fileName = $body['filename'];
        $file = $this->path['transfer'] . $fileName;
        $fileInfo = pathinfo($file);

        $directory = "{$this->path['transfer']}{$fileInfo['filename']}/";
        if(!is_dir($directory)){
            mkdir($directory, 0755, true);
        }
        $directoryTarget = "{$this->path['transfer']}{$fileInfo["filename"]}.{$fileInfo["extension"]}";

        $zip = new \Chumper\Zipper\Zipper;
        $zip->make($directoryTarget)->extractTo($directory);
        
        // ==== move json ====
        $jsonFiles = scandir($directory."json/");
        $jsonSource = $directory."json/";
        $jsonDest =  $this->path['data'];
        $this->CopyFolder($jsonSource, $jsonDest);

        // ==== move images ====
        // $imageFiles = scandir($directory."images/");
        // $imageSource = $directory."images/";
        // $imageDest =  '../public/img/';
        // $this->CopyFolder($imageSource, $imageDest);
        
        return true;
    }

    function GetData()
    {
        echo json_encode([
            $this->GetCategories()
        ]);
    }

    function GetPages()
    {
        $data = file_get_contents("{$this->path['data']}pages.json");
        $data = json_decode($data, true);
        $data = collect($data);
        return $data;
    }

    function GetCategories()
    {
        $pivot = $this->GetProductCategories();
        $data = file_get_contents("{$this->path['data']}categories.json");
        $data = json_decode($data, true);
        $data = collect($data);

        $data->transform(function($item) use ($pivot) {
            $item["productIds"] = $pivot->where('category_id', $item['id'])->pluck('product_id');
            return $item;
        });
        return $data;
    }

    function GetProducts()
    {
        $pivot = $this->GetProductCategories();
        $data = file_get_contents("{$this->path['data']}products.json");
        $data = json_decode($data, true);
        $data = collect($data);
        return $data;
    }

    function GetProductCategories()
    {
        $data = file_get_contents("{$this->path['data']}products_categories.json");
        $data = json_decode($data, true);
        $data = collect($data);
        return $data;
    }

    function GetBlogs()
    {
        $data = file_get_contents("{$this->path['data']}blogs.json");
        $data = json_decode($data, true);
        $data = collect($data);
        return $data;
    }

    function CopyFolder($source, $target)
    {
        $directory = opendir($source);
        @mkdir($target);
        while( $file = readdir($directory) ) {  
            if (( $file != '.' ) && ( $file != '..' )) {  
                if ( is_dir($source . '/' . $file) )  
                {  
                    $this->CopyFolder($source . '/' . $file, $target . '/' . $file);  
                }  
                else {  
                    copy($source . '/' . $file, $target . '/' . $file);  
                }  
            }  
        }
        closedir($directory); 
    }

    function PostAppointment($query)
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];

        try {
            
            $mail = $this->GetMailConfig();
            $mail->Subject = '[cupid] Make Appointment';
            $mail->Body    = "
                <table>
                <tr>
                    <td>Name</td>
                    <td>{$query["name"]}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{$query["email"]}</td>
                </tr>
                <tr>
                    <td>Mobile</td>
                    <td>{$query["mobile"]}</td>
                </tr>
                <tr>
                    <td>Message</td>
                    <td>{$query["message"]}</td>
                </tr>
                </table>
            ";

            $mail->send();
            
            $response["status"] = "success";
            $response["message"] = "";
        } catch (Exception $e) {
            $response["status"] = "fail";
            $response["message"] = $mail->ErrorInfo;
        }

        echo json_encode($response);
    }

    function GetMailConfig()
    {
        $mail = new PHPMailer(true);

        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $mail->isSMTP();
        $mail->SMTPAuth   = true;
        $mail->Host       = $this->config["mail"]["host"];
        $mail->Username   = $this->config["mail"]["username"];
        $mail->Password   = $this->config["mail"]["password"];
        $mail->Port       = $this->config["mail"]["port"];
        $mail->setFrom($this->config["mail"]["from"]);
        $mail->addAddress($this->config["mail"]["to"]);
        $mail->isHTML(true);

        return $mail;
    }

}