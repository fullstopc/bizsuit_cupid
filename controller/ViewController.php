<?php

use Philo\Blade\Blade;
use Tightenco\Collect;

class ViewController
{    
    protected $config;
    protected $helper;
    protected $path;

    function __construct()
    {
        $this->path = [
            'view' => realpath('./../resources/views/') . '/',
            'cache' => realpath('./../resources/cache/'). '/',
            'transfer' => realpath('./../resources/transfer/'). '/',
            'data' => realpath('./../resources/data/'). '/',
        ];
        $this->config = include_once '../config.php';
        $this->helper = new HelperController($this->config, $this->path);
    }

    function view($view = null, $data = [], $mergeData = [])
    {
        $views = $this->path['view'];
        $cache = $this->path['cache'];
        $blade = new Blade($views, $cache);

        if(!$blade->view()->exists($view))
        {
            header("HTTP/1.0 404 Not Found");
            die;
        }

        // add config
        $data["config"] = $this->config;
        
        return $blade->view()->make($view, $data, $mergeData);
    }

    function GetIndex()
    {
        $pages = $this->helper->GetPages();
        $page = $pages->firstWhere('url', 'home');

        $banners = [];
        if(array_key_exists("banners", $page) && count($page['banners']) > 0)
        {
            $banners = collect($page['banners'])->map(function($banner) use ($page){
                return [
                    'desktop' => "/img/content/{$page['id']}/{$banner['desktop_banner']['image']}",
                    'mobile' => "/img/content/{$page['id']}/{$banner['mobile_banner']['image']}",
                ];
            });
        }

        echo $this->view('page', [
            'page' => $page
        ]);
    }

    function GetPage($pageUrl)
    {
        $pages = $this->helper->GetPages();
        $page = $pages->firstWhere('url', $pageUrl);

        if($page == null){
            header("HTTP/1.0 404 Not Found");
            die;
        }

        $imageUrl = null;
        if(array_key_exists("banners", $page) && count($page['banners']) > 0)
        {
            if(array_key_exists("desktop_banner", $page['banners'][0]))
            {
                $imageUrl = $page['banners'][0]['desktop_banner']['path'] . $page['banners'][0]['desktop_banner']['image'];
            }
        }

        echo $this->view('subpage', [
            'page' => $page,
            'imageUrl' => $imageUrl
        ]);
    }

    function GetCategory($categoryCode, $query = null)
    {
        $categories = $this->helper->GetCategories();
        $products = $this->helper->GetProducts()->map(function($product){
            $product['variant'] = $product['variants'][0];
            return $product;
        });
        $productCategories = $this->helper->GetProductCategories();

        $pageSort = array_key_exists("page_sort", $query) ? $query["page_sort"] : "name_asc";
        $pageLength = array_key_exists("page_length", $query) ? intval($query["page_length"]) : 15;
        $pageIndex = array_key_exists("page_index", $query) ? intval($query["page_index"]) : 1;

        $pageIndex = $pageIndex == 0 ? 1 : $pageIndex;
        $pageLength = $pageLength == 0 ? 15 : $pageLength;

        $params = [
            "page_sort" => $pageSort,
            "page_length" => $pageLength,
            "page_index" => $pageIndex
        ];

        $categories = $categories->filter(function($category) use ($categoryCode){
            return $category['code'] === $categoryCode;
        });
        

        // map product full info
        $categories->transform(function($category, $key) use ($products){
            $productIds = $category['productIds']->toArray();
            $category['products'] = $products->filter(function($product) use ($productIds) {
                return in_array($product['id'], $productIds);
            });
            return $category;
        });
        
        // add empty banner
        $categories->transform(function($category, $key){
            if(!array_key_exists("banners", $category)){
                $category['banners'] = [];
            }
            if(!array_key_exists("banner", $category)){
                if(count($category['banners']) > 0){
                    $category['banner'] = $category['banners'][0];
                }
                else{
                    $category['banner'] = null;
                }
            }
            
            
            return $category;
        });

        // sorting
        $categories->transform(function($category, $key) use ($pageSort) {
            switch($pageSort){
                case "price_asc":
                    $category['products'] = collect($category['products'])->sortBy('variant.price');
                    break;
                case "price_desc":
                    $category['products'] = collect($category['products'])->sortByDesc('variant.price');
                    break;
                case "name_asc":
                    $category['products'] = collect($category['products'])->sortBy('title');
                    break;
                case "name_desc":
                    $category['products'] = collect($category['products'])->sortByDesc('title');
                    break;
            }
            return $category;
        });
        
        $sorting = [
            "links" => [
                ["code"=>"name_asc","label"=>"Name (A - Z)","url" => "/collections/$categoryCode?page_sort=name_asc&page_length={$params['page_length']}&page_index={$params['page_index']}"],
                ["code"=>"name_desc","label"=>"Name (Z - A)","url" => "/collections/$categoryCode?page_sort=name_desc&page_length={$params['page_length']}&page_index={$params['page_index']}"],
                ["code"=>"price_asc","label"=>"Name (Low to High)","url" => "/collections/$categoryCode?page_sort=price_asc&page_length={$params['page_length']}&page_index={$params['page_index']}"],
                ["code"=>"price_desc","label"=>"Price (High to Low)","url" => "/collections/$categoryCode?page_sort=price_desc&page_length={$params['page_length']}&page_index={$params['page_index']}"],
            ],
            "current" => $params["page_sort"]
        ];

        // total product = 
        $productLength = $categories->reduce(function($carry, $item){
            return $carry + count($item["products"]);
        });
        $pageTotalLength = ceil($productLength/$pageLength);
        $pageTotalLength = $pageTotalLength == 0 ? 1 : $pageTotalLength;
        $links = collect(range(0,$pageTotalLength - 1))->map(function($item, $key) use ($categoryCode, $params){
            $query = array_merge(array(), $params); //clone
            $query["page_index"] = $key + 1;
            return "/collections/$categoryCode?".http_build_query($query);
        });
        $pagination = [
            "links" => $links,
            "total" => $productLength,
            "current_page" => $pageIndex,
            "last_page" => $pageTotalLength,
        ];
        $prevPageIndex = $pageIndex > 1 ? $pageIndex - 1 : 1;
        $nextPageIndex = $pageTotalLength > $pageIndex ? $pageIndex + 1 : $pageTotalLength;
        $pagination["first_page_url"] = "/collections/$categoryCode?page_sort={$params['page_sort']}&page_length={$params['page_length']}&page_index=1";
        $pagination["last_page_url"] = "/collections/$categoryCode?page_sort={$params['page_sort']}&page_length={$params['page_length']}&page_index={$pageTotalLength}";
        $pagination["prev_page_url"] = "/collections/$categoryCode?page_sort={$params['page_sort']}&page_length={$params['page_length']}&page_index={$prevPageIndex}";
        $pagination["next_page_url"] = "/collections/$categoryCode?page_sort={$params['page_sort']}&page_length={$params['page_length']}&page_index={$nextPageIndex}";
        
        // data length
        $categories->transform(function($category, $key) use ($pageLength, $pageIndex) {
            if(count($category["products"]) > 0){
                $products = collect($category['products']);
                $productPieces = $products->chunk($pageLength);
                $category['products'] = $productPieces[$pageIndex - 1];
            }
            return $category;
        });
        
        $categories = $categories->values();
        $category = ($categories->count() > 0) ? $categories[0] : null;
        
        echo $this->view('product', [
            'category' => $category,
            'pagination' => $pagination,
            'sorting' => $sorting,
        ]);
    }
    
    function GetProduct($categoryCode, $productUrl)
    {
        $categories = $this->helper->GetCategories();
        $products = $this->helper->GetProducts();

        $category = $categories->firstWhere('code',$categoryCode);
        $product = $products->firstWhere('url',$productUrl);
        $product['variant'] = $product['variants'][0];
        
        $product['reference'] = null;
        $product['specs'] = null;

        if($product['custom_content'] != null && $product['custom_content']['properties'] != null)
        {
            $properties = collect($product['custom_content']['properties']);
            $reference = $properties->firstWhere('group','reference');
            $specs = $properties->where('group','specs');

            if($specs != null) $product['specs'] = $specs;
            if($reference != null) $product['reference'] = $reference;
        }

        echo $this->view('product-detail', [
            'product' => $product,
            'category' => $category,
        ]);
    }

    function GetBlogs()
    {
        $blogs = $this->helper->GetBlogs();
        $blogs->transform(function($blog){
            if(count($blog["images"]) > 0)
            {
                $blog["image"] = $blog["images"][0]["image"];
            }
            return $blog;
        });

        echo $this->view('blog', [
            "blogs" => $blogs
        ]);
    }

    function GetBlog($blogUrl)
    {
        $blogs = $this->helper->GetBlogs();
        $blog = $blogs->firstWhere("url", $blogUrl);
        $blogTags = explode(',',$blog['tags']);

        echo $this->view('blog-detail', [
            "blog" => $blog,
            "blogTags" => $blogTags
        ]);
    }

    function GetContact()
    {
        $pages = $this->helper->GetPages();
        $contact = $pages->firstWhere('url', 'contact-us');
        echo $this->view('contact-us', [
            "pageTitle" => $contact['title'],
            "pageBody" => $contact['body']
        ]);
    }

    function GetAppointment()
    {
        echo $this->view('appointment', [

        ]);
    }

    function GetGallery()
    {
        // $pages = $this->helper->GetPages();
        // $page = $pages->firstWhere('url', 'gallery');

        $blogs = $this->helper->GetBlogs();
        $blog = $blogs->firstWhere("group", 'gallery');
        
        echo $this->view('gallery', [
            'page' => $blog,
        ]);
    }

    function GetTestimonial()
    {
        $testimonials = [];
        

        echo $this->view('testimonial', [
            "testimonials" => $testimonials
        ]);
    }
}